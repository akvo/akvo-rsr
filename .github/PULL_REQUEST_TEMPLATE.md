---
title: '[#IssueID\] : Feature Request: '
labels: Needs Review
assignees: 'AkvoAnt'

---

<!--
For internal contributors, before work can start on a PR that's user facing,
the "Test plan" will have to be reviewed.
-->


# TODO / Done

Summarize what has been changed / what has to be done in order to finalize the PR.

 - [ ] Refactor X
 - [ ] Extend Y to do Z
 - [ ] etc.

# Test plan

What tests are necessary to ensure this works or doesn't break anything working

 - [ ] Happy path
 - [ ] Scenario: User is super-fast
 
## Happy path

 - Click on A
 - Drag B to C
 - ...
